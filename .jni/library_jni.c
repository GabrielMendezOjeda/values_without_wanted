#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "library.c"
extern JNIEnv *javaEnv;

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

JNIEXPORT jintArray JNICALL Java_library_valuesWithoutWanted_1
  (JNIEnv *env, jobject object, jint wanted, jintArray value, jint valueLength, jobject valuesWithoutWantedLength)
{
    javaEnv = env;
    int c_wanted = toInt(wanted);
    int c_valueLength = toInt(valueLength);
    int* c_value = toIntArray(value, c_valueLength);
    int* c_valuesWithoutWantedLength = intPtr(valuesWithoutWantedLength);
    int* c_outValue = valuesWithoutWanted(c_wanted, c_value, c_valueLength, c_valuesWithoutWantedLength);
    setJintArray(value , c_value, &c_valueLength);
    setIntegerValue(valuesWithoutWantedLength, c_valuesWithoutWantedLength);
    return toJintArray(c_outValue, c_valuesWithoutWantedLength);
}
