/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

public class library {
    @executerpane.MethodAnnotation(signature = "valuesWithoutWanted(int,int*,int,int*):int*")
    public int[] valuesWithoutWanted(int wanted, int[] value, int valueLength, Integer valuesWithoutWantedLength){
        if (valuesWithoutWantedLength == null){
            throw new IllegalArgumentException("expected valuesWithoutWantedLength index");
        }
        return valuesWithoutWanted_(wanted, value, valueLength, valuesWithoutWantedLength);
    }
    private native int[] valuesWithoutWanted_(int wanted, int[] value, int valueLength, Integer valuesWithoutWantedLength);


    static {
        System.load(new java.io.File(".jni", "library_jni.so").getAbsolutePath());
    }
}
