/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#ifndef _jni_test_c
#define _jni_test_c

#include "jni_test.h"
#include <stdarg.h>

extern JNIEnv *javaEnv;

void assertEquals__(const char *format, ...) {}
void assertTrue__(bool actual) {}
void assertFalse__(bool actual) {}

void setAssertInfo_(const char* file, const char* function, int line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LTestResultPanel;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "setAssertInfo", "(Ljava/lang/String;Ljava/lang/String;I)V");
	(*javaEnv)->CallStaticIntMethod(javaEnv, cls, valueOf, toJstring(file), toJstring(function), toJint(line));
}

void assertEqualsObject_(jobject expected, jobject actual) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertEquals", "(Ljava/lang/Object;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, expected, actual);
}

void fail_(const char* message) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "fail", "(Ljava/lang/String;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJstring(message));
}

void assertEquals_int_(int expected, int actual) {
	assertEqualsObject_(newIntegerObject(&expected), newIntegerObject(&actual));
}

void assertEquals_byte_(byte expected, byte actual) {
	assertEqualsObject_(newByteObject(&expected), newByteObject(&actual));
}

void assertEquals_short_(short expected, short actual) {
	assertEqualsObject_(newShortObject(&expected), newShortObject(&actual));
}

void assertEquals_long_(long expected, long actual) {
	assertEqualsObject_(newLongObject(&expected), newLongObject(&actual));
}

void assertEquals_float_(float expected, float actual, float delta) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertEquals", "(FFF)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, expected, actual, delta);
}

void assertEquals_double_(double expected, double actual, double delta) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertEquals", "(DDD)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, expected, actual, delta);
}

void assertEquals_bool_(bool expected, bool actual) {
	assertEqualsObject_(newBooleanObject(&expected), newBooleanObject(&actual));
}

void assertTrue_(bool actual) {
	assertEquals_bool_(true, actual);
}

void assertFalse_(bool actual) {
	assertEquals_bool_(false, actual);
}

void assertEquals_char_(char expected, char actual) {
	assertEqualsObject_(newCharacterObject(&expected), newCharacterObject(&actual));
}

void assertEquals_String_(const char* expected, const char* actual) {
	assertEqualsObject_(toJstring(expected), toJstring(actual));
}

void assertArrayEquals_int_(int* expected, int expectedLength, int* actual, int actualLength) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([I[I)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJintArray(expected, &expectedLength), toJintArray(actual, &actualLength));
}

void assertArrayEquals_byte_(byte* expected, int expectedLength, byte* actual, int actualLength) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([B[B)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJbyteArray(expected, &expectedLength), toJbyteArray(actual, &actualLength));
}

void assertArrayEquals_short_(short* expected, int expectedLength, short* actual, int actualLength) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([S[S)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJshortArray(expected, &expectedLength), toJshortArray(actual, &actualLength));
}

void assertArrayEquals_long_(long* expected, int expectedLength, long* actual, int actualLength) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([J[J)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJlongArray(expected, &expectedLength), toJlongArray(actual, &actualLength));
}

void assertArrayEquals_float_(float* expected, int expectedLength, float* actual, int actualLength, float delta) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([F[FF)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJfloatArray(expected, &expectedLength), toJfloatArray(actual, &actualLength), delta);
}

void assertArrayEquals_double_(double* expected, int expectedLength, double* actual, int actualLength, double delta) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([D[DD)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJdoubleArray(expected, &expectedLength), toJdoubleArray(actual, &actualLength), delta);
}

void assertArrayEquals_bool_(bool* expected, int expectedLength, bool* actual, int actualLength) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Z[Z)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJbooleanArray(expected, &expectedLength), toJbooleanArray(actual, &actualLength));
}

void assertArrayEquals_char_(char* expected, int expectedLength, char* actual, int actualLength) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([C[C)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJcharArray(expected, &expectedLength), toJcharArray(actual, &actualLength));
}

void assertArrayEquals_String_(const char** expected, int expectedLength, const char** actual, int actualLength) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJstringArray(expected, &expectedLength), toJstringArray(actual, &actualLength));
}

void assertMatrixEquals_int_(int** expected, int expectedRows, int expectedColumns, int** actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJintMatrix(expected, &expectedRows, &expectedColumns), toJintMatrix(actual, &actualRows, &actualColumns));
}

void assertMatrixEquals_byte_(byte** expected, int expectedRows, int expectedColumns, byte** actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJbyteMatrix(expected, &expectedRows, &expectedColumns), toJbyteMatrix(actual, &actualRows, &actualColumns));
}

void assertMatrixEquals_short_(short** expected, int expectedRows, int expectedColumns, short** actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJshortMatrix(expected, &expectedRows, &expectedColumns), toJshortMatrix(actual, &actualRows, &actualColumns));
}

void assertMatrixEquals_long_(long** expected, int expectedRows, int expectedColumns, long** actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJlongMatrix(expected, &expectedRows, &expectedColumns), toJlongMatrix(actual, &actualRows, &actualColumns));
}

void assertMatrixEquals_float_(float** expected, int expectedRows, int expectedColumns, float** actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJfloatMatrix(expected, &expectedRows, &expectedColumns), toJfloatMatrix(actual, &actualRows, &actualColumns));
}

void assertMatrixEquals_double_(double** expected, int expectedRows, int expectedColumns, double** actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJdoubleMatrix(expected, &expectedRows, &expectedColumns), toJdoubleMatrix(actual, &actualRows, &actualColumns));
}

void assertMatrixEquals_bool_(bool** expected, int expectedRows, int expectedColumns, bool** actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJbooleanMatrix(expected, &expectedRows, &expectedColumns), toJbooleanMatrix(actual, &actualRows, &actualColumns));
}

void assertMatrixEquals_char_(char** expected, int expectedRows, int expectedColumns, char** actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJcharMatrix(expected, &expectedRows, &expectedColumns), toJcharMatrix(actual, &actualRows, &actualColumns));
}

void assertMatrixEquals_String_(const char*** expected, int expectedRows, int expectedColumns, const char*** actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJstringMatrix(expected, &expectedRows, &expectedColumns), toJstringMatrix(actual, &actualRows, &actualColumns));
}

void assertMatrixRegionEquals_int_(int* expected, int expectedRows, int expectedColumns, int* actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJintMatrixRegion(expected, &expectedRows, &expectedColumns), toJintMatrixRegion(actual, &actualRows, &actualColumns));
}

void assertMatrixRegionEquals_byte_(byte* expected, int expectedRows, int expectedColumns, byte* actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJbyteMatrixRegion(expected, &expectedRows, &expectedColumns), toJbyteMatrixRegion(actual, &actualRows, &actualColumns));
}

void assertMatrixRegionEquals_short_(short* expected, int expectedRows, int expectedColumns, short* actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJshortMatrixRegion(expected, &expectedRows, &expectedColumns), toJshortMatrixRegion(actual, &actualRows, &actualColumns));
}

void assertMatrixRegionEquals_long_(long* expected, int expectedRows, int expectedColumns, long* actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJlongMatrixRegion(expected, &expectedRows, &expectedColumns), toJlongMatrixRegion(actual, &actualRows, &actualColumns));
}

void assertMatrixRegionEquals_float_(float* expected, int expectedRows, int expectedColumns, float* actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJfloatMatrixRegion(expected, &expectedRows, &expectedColumns), toJfloatMatrixRegion(actual, &actualRows, &actualColumns));
}

void assertMatrixRegionEquals_double_(double* expected, int expectedRows, int expectedColumns, double* actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJdoubleMatrixRegion(expected, &expectedRows, &expectedColumns), toJdoubleMatrixRegion(actual, &actualRows, &actualColumns));
}

void assertMatrixRegionEquals_bool_(bool* expected, int expectedRows, int expectedColumns, bool* actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJbooleanMatrixRegion(expected, &expectedRows, &expectedColumns), toJbooleanMatrixRegion(actual, &actualRows, &actualColumns));
}

void assertMatrixRegionEquals_char_(char* expected, int expectedRows, int expectedColumns, char* actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJcharMatrixRegion(expected, &expectedRows, &expectedColumns), toJcharMatrixRegion(actual, &actualRows, &actualColumns));
}

void assertMatrixRegionEquals_String_(const char** expected, int expectedRows, int expectedColumns, const char** actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJstringMatrixRegion(expected, &expectedRows, &expectedColumns), toJstringMatrixRegion(actual, &actualRows, &actualColumns));
}

#endif
