/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

int* valuesWithoutWanted(int wanted, int* value, int valueLength, int* valuesWithoutWantedLength) {
    int* valuesWithoutWantedResult = (int*) malloc(1 * sizeof(int));
    valuesWithoutWantedResult[0] = -1000;
    *valuesWithoutWantedLength = 1;
    return valuesWithoutWantedResult;
}
